# Prometheus Demo Service

Forked from upstream: https://github.com/juliusv/prometheus_demo_service 

Check the [blog post](https://dnsmichi.at/2021/09/14/monitoring-kubernetes-with-prometheus-and-grafana-free-workshop/) for more details on the Kubernetes monitoring workshop. 

## Changes

- `main` branch
- Docker image uses Debian
- GitLab CI/CD config to build binary, build and upload the Docker image to the GitLab container registry


